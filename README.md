# Kata TDD : FizzBuzz

## Énoncé du fizz buzz adapté

Ecrire un programme qui transform un chiffre à sa chaine de caractère "Fizz Buzz" équivalente.

### Règles 

Chaque chiffre est transformé en chaine de caractère.

De plus, si le chiffre est :
- multiple de trois, il est remplacé par le mot "Fizz".
- multiple de cinq, il est remplacé par le mot "Buzz".
- multiple de trois et cinq, il est remplacé par le mot "FizzBuzz".
