package fr.daveo.service;

public class FizzBuzzService {

    private static final String FIZZBUZZ = "fizzbuzz";
    private static final String FIZZ = "fizz";
    private static final String BUZZ = "buzz";

    public static String formatNumberToFizzBuzz(int numberToFormat) {
        if(shouldReturnFizzBuzz(numberToFormat))
            return FIZZBUZZ;

        if(shouldReturnFizz(numberToFormat))
            return FIZZ;

        if(shouldReturnBuzz(numberToFormat))
            return BUZZ;

        return String.valueOf(numberToFormat);
    }

    private static boolean shouldReturnFizzBuzz(int numberToFormat) {
        return numberToFormat % 15 == 0;
    }

    private static boolean shouldReturnBuzz(int numberToFormat) {
        return numberToFormat % 5 == 0;
    }

    private static boolean shouldReturnFizz(int numberToFormat) {
        return numberToFormat % 3 == 0;
    }
}
