
import fr.daveo.service.FizzBuzzService;

import org.junit.jupiter.api.DisplayNameGeneration;
import org.junit.jupiter.api.DisplayNameGenerator;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;

@DisplayNameGeneration(DisplayNameGenerator.ReplaceUnderscores.class)
public class FizzBuzzTest {

    private static final int NUMBER_THREE = 3;
    private static final int NUMBER_FIVE = 5;
    private static final int NUMBER_FIFTEEN = 15;
    private static final int NEGATIVE_NUMBER_ONE = -1;
    private static final int NEGATIVE_NUMBER_THREE = -3;

    private static final String EXPECTED_NUMBER_ONE = "1";
    private static final String EXPECTED_FIZZ_STRING = "fizz";
    private static final String EXPECTED_BUZZ_STRING = "buzz";
    private static final String EXPECTED_FIZZBUZZ_STRING = "fizzbuzz";
    private static final String EXPECTED_NEGATIVE_NUMBER_ONE_STRING = "-1";

    @Test
    void should_return_number_one_when_format_fizz_buzz_given_number_one() {
        // Given
        int numberOne = 1;

        // When
        String result = FizzBuzzService.formatNumberToFizzBuzz(numberOne);

        // Then
        assertThat(result).isEqualTo(EXPECTED_NUMBER_ONE);
    }

    @Test
    void should_return_fizz_string_when_format_number_to_fizz_buzz_given_multiple_of_three_like_a_three_number() {
        // Given / When
        String result = FizzBuzzService.formatNumberToFizzBuzz(NUMBER_THREE);

        // Then
        assertThat(result).isEqualTo(EXPECTED_FIZZ_STRING);
    }

    @Test
    void should_return_buzz_string_when_format_number_to_fizz_buzz_given_multiple_of_five_like_a_five_number() {
        // Given / When
        String result = FizzBuzzService.formatNumberToFizzBuzz(NUMBER_FIVE);

        // Then
        assertThat(result).isEqualTo(EXPECTED_BUZZ_STRING);
    }

    @Test
    void should_return_fizz_buzz_string_when_format_number_to_fizz_buzz_given_multiple_of_three_and_five_like_a_fifteen_number() {
        // Given / When
        String result = FizzBuzzService.formatNumberToFizzBuzz(NUMBER_FIFTEEN);

        // Then
        assertThat(result).isEqualTo(EXPECTED_FIZZBUZZ_STRING);
    }

    @Test
    void should_return_negative_number_one_string_when_format_number_to_fizz_buzz_given_negative_number_one() {
        // Given / When
        String result = FizzBuzzService.formatNumberToFizzBuzz(NEGATIVE_NUMBER_ONE);

        // Then
        assertThat(result).isEqualTo(EXPECTED_NEGATIVE_NUMBER_ONE_STRING);
    }

    @Test
    void should_return_fizz_string_when_format_number_to_fizz_buzz_given_multiple_of_three_like_a_negative_number_three() {
        // Given / When
        String result = FizzBuzzService.formatNumberToFizzBuzz(NEGATIVE_NUMBER_THREE);

        // Then
        assertThat(result).isEqualTo(EXPECTED_FIZZ_STRING);
    }
}
